﻿using System;
using System.IO;

namespace NewtInfo {
    public static class Util {
        public static void LogError(Exception e) {
            try {
                using (StreamWriter writer = new StreamWriter("errors.txt", true)) {
                    writer.WriteLine("[" + DateTime.Now.ToString() + "] Error: " + e.Message + ", Source: " + e.Source + ", Stack: " + e.StackTrace);
                    if (e.InnerException != null) {
                        writer.WriteLine("(" + e.InnerException.Message + ":" + e.InnerException.StackTrace + ")");
                    }
                    writer.Close();
                }
            }
            catch  {}
        }

        public static void WriteToChat(string msg) {
            try  { Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + msg, 5); }  // TODO: hardcoded "5"
            catch (Exception exc) { LogError(exc); }
        }
    }
}