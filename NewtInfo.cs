﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;

/**
 * 2024 Brian Wright. Adapted from Mag-nus VVS template, available on GitHub.
 * 
 * Public domain, use as you wish.
 * 
 * If you have issues compiling, replace the Decal.Adapter and VirindiViewService references.
 * Decal.Adapter could be in C:\Games\Decal\
 * VirindiViewService could be in C:\Games\VirindiPlugins\VirindiViewService\
 */

namespace NewtInfo {
    // Attaches events from core
    [WireUpBaseEvents]

    // View (UI) handling
    [MVView("NewtInfo.NewtView.xml")]
    [MVWireUpControlEvents]

    [FriendlyName("NewtInfo")]
    public class PluginCore : PluginBase {
        protected override void Startup() {
            try {
                Globals.Init("NewtInfo", Host, Core);
                MVWireupHelper.WireupStart(this, Host);
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        protected override void Shutdown() {
            try {
                MVWireupHelper.WireupEnd(this);
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [BaseEvent("LoginComplete", "CharacterFilter")]
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                Util.WriteToChat("Plugin now online. Server population: " + Core.CharacterFilter.ServerPopulation);
                Util.WriteToChat("CharacterFilter_LoginComplete");

                InitSampleList();  // TODO-hi: rename

                // Subscribe to events here
                Globals.Core.WorldFilter.ChangeObject += new EventHandler<ChangeObjectEventArgs>(WorldFilter_ChangeObject2);  // TODO: which fn and args?
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [BaseEvent("Logoff", "CharacterFilter")]
        private void CharacterFilter_Logoff(object sender, Decal.Adapter.Wrappers.LogoffEventArgs e) {
            try {
                Globals.Core.WorldFilter.ChangeObject -= new EventHandler<ChangeObjectEventArgs>(WorldFilter_ChangeObject2);  // TODO: should match other function. wtf, is this real code? "minus equals" and "new"??
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [BaseEvent("ChangeObject", "WorldFilter")]
        void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs e) {
            try {
                if (e.Change == WorldChangeType.IdentReceived) {
                    Util.WriteToChat("WorldFilter_ChangeObject: " + e.Changed.Name + " " + e.Change);
                }
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        void WorldFilter_ChangeObject2(object sender, ChangeObjectEventArgs e)  {}

        [MVControlEvent("UseSelectedItem", "Click")]
        void UseSelectedItem_Click(object sender, MVControlEventArgs e) {
            try {
                if (Globals.Host.Actions.CurrentSelection == 0 || Globals.Core.WorldFilter[Globals.Host.Actions.CurrentSelection] == null) {
                    Util.WriteToChat("UseSelectedItem no item selected");
                    return;
                }

                Util.WriteToChat("UseSelectedItem " + Globals.Core.WorldFilter[Globals.Host.Actions.CurrentSelection].Name);
                Globals.Host.Actions.UseItem(Globals.Host.Actions.CurrentSelection, 0);
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [MVControlEvent("ToggleAttack", "Click")]
        void ToggleAttack_Click(object sender, MVControlEventArgs e) {
            try {
                Util.WriteToChat("ToggleAttack");

                if (Globals.Host.Actions.CombatMode == CombatState.Peace) {
                    Globals.Host.Actions.SetCombatMode(CombatState.Melee);
                } else {
                    Globals.Host.Actions.SetCombatMode(CombatState.Peace);
                }
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [MVControlEvent("DoesNothing", "Click")]
        void DoesNothing_Click(object sender, MVControlEventArgs e) {
            try  {}
            catch(Exception exc)  { Util.LogError(exc); }
        }

        [MVControlReference("SampleList")]
        private IList SampleList = null;

        private void InitSampleList() {
            foreach (WorldObject worldObject in Globals.Core.WorldFilter.GetByContainer(Globals.Core.CharacterFilter.Id)) {
                IListRow row = SampleList.Add();

                row[0][1] = worldObject.Icon + 0x6000000; // [0][1] used for DecalControls.IconColumn column.
                row[1][0] = worldObject.Name;

                // An alternate method to create an empty column is to add to the view XML:
                // <column progid="DecalControls.TextColumn" name="colF" />
                // It is column index 5 and has no size associated with it. You can use this column to store an id of an item, or other misc data that you can use
                // later to grab info about the row, or maybe its sort order, etc..

                // To clear the list: SampleList.Clear();

                // If we want to check if this item is equipped, we could do the following
                if (worldObject.Values(LongValueKey.EquippedSlots) > 0)  {}

                // This check will pass if the object is wielded
                // Take note that someone else might be wielding this object. If you want to know if its wielded by YOU, you need to add another check.
                if (worldObject.Values(LongValueKey.Slot, -1) == -1)  {}

                // You can get an items current mana, but only if the item has been id'd, otherwise it will just return 0.
                if (worldObject.HasIdData) {
                    int currentMana = worldObject.Values(LongValueKey.CurrentMana);
                }

                // But also note that we don't know how long ago this item has been id'd. Maybe it was id'd an hour ago? The mana data would be erroneous.
                // So, we could get fresh id data for the object with the following:
                // Globals.Host.Actions.RequestId(worldObject.Id);

                // But now note that it may take a second or so to get that id data. So if we did the following:
                // Globals.Host.Actions.RequestId(worldObject.Id);
                // worldObject.Values(LongValueKey.CurrentMana) <-- This would still be the OLD information and not the new because the above statement hasn't finished.
            }
        }

        [MVControlReference("SampleListText")]
        private IStaticText SampleListText = null;

        [MVControlReference("SampleListCheckBox")]
        private ICheckBox SampleListCheckBox = null;

        [MVControlEvent("SampleListCheckBox", "Change")]
        void SampleListCheckBox_Change(object sender, MVCheckBoxChangeEventArgs e) {
            try {
                Util.WriteToChat("SampleListCheckBox_Change " + SampleListCheckBox.Checked);
                SampleListText.Text = SampleListCheckBox.Checked.ToString();
            }
            catch(Exception exc)  { Util.LogError(exc); }
        }
    }
}