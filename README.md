# NewtInfo

This is the _New Target Info_ plugin source.


## Development

- [ ] reimplement/port functions
    - [ ] **clsDatabaseEditor.cls**
        - [ ] (sub) Class_Initialize
        - [ ] (sub) Class_Terminate
        - [ ] (public event) Done
        - [ ] DataView : View
        - [ ] lstCritters : DecalControls.List
        - [ ] (enum) cCritters
        - [ ] btnOK : DecalControls.PushButton
        - [ ] btnCancel : DecalControls.PushButton
        - [ ] btnApply : DecalControls.PushButton
        - [ ] (sub) btnOK_Accepted
        - [ ] (sub) btnCancel_Accepted
        - [ ] (sub) btnApply_Accepted
        - [ ] (sub) lstCritters_Change
    - [ ] **clsDataParser.cls**
        - [ ] (sub) Class_Initialize
        - [ ] (sub) Class_Terminate
        - [ ] (public event) Done
        - [ ] (public event) AbortError
        - [ ] (public sub) Start
        - [ ] tmFauna : DecalInput.Timer
        - [ ] tmSpecies : DecalInput.Timer
        - [ ] xmlIn, xmlOut, nodeFauna, nodeCreature, nodeSpecies, nodeProt
        - [ ] i, j, lRating, bFound, sSpeciesName, sCreatureName
        - [ ] nodeItrFauna : ~, nodeItrSpecies : ~
        - [ ] colSpecies : ~
        - [ ] Vuln
        - [ ] arrRatings(0--7), lBestRating, sRating
        - [ ] (sub) tmFauna_Timeout
        - [ ] (sub) SpeciesLoop
        - [ ] (sub) tmSpecies_Timeout
        - [ ] (sub) Finish
        - [ ] (sub) DoAbortError
    - [ ] **clsVulnDbase.cls**
        - [ ] (sub) Class_Initialize
        - [ ] (sub) Class_Terminate
        - [ ] (public sub) Load
        - [ ] (public prop getter) Item
        - [ ] dctCreatures : Dictionary
        - [ ] dctSpecies : Dictionary
        - [ ] dctIDedCache : Dictionary
        - [ ] (public sub) UpdateCtText
        - [ ] (public sub) SaveCache
    - [ ] **clsVulnInfo.cls**
        - [ ] (sub) Class_Initialize
        - [ ] (public) CreatureName : String
        - [ ] (public) SpeciesName : String
        - [ ] (public) CreatureID : String
        - [ ] (public) SpeciesID : String
        - [ ] (public enum) eVulnType
        - [ ] (public enum) eRating
        - [ ] myVulns : eRating
        - [ ] myBestVulns : Collection
        - [ ] bHasFoundBestVulns
        - [ ] mySortedVulns : eVulnType
        - [ ] bHasSortedVulns
        - [ ] (public) AttackHeight : String
        - [ ] (public) IsSpeciesData
        - [ ] (public prop let) VulnRating
        - [ ] (public prop getter) VulnRating
        - [ ] (public fn) VulnRatingString
        - [ ] (public fn) BestVulns
        - [ ] (public fn) SortedVulns
        - [ ] (public fn) HasVulnInfo
    - [ ] **Data.bas**
        - [ ] (public sub) InitData
        - [ ] (public sub) TerminateData
        - [ ] dctEffects : Dictionary
        - [ ] dctSpellWords : Dictionary
        - [ ] dctSpecies : Dictionary
        - [ ] (public) arrRatingNames : String
        - [ ] (public) arrDbRatingNames : String
        - [ ] (public) RatingColors
        - [ ] (public) arrVulnNames : String
        - [ ] (public) arrDbVulnNames : String
        - [ ] (public) VulnIcons
        - [ ] (public fn) SpellWordsToParErr
        - [ ] (public fn) DebuffInfo
        - [ ] (public fn) SpeciesNameToID
        - [ ] (public fn) EffectsArray
        - [ ] (public fn) DbStrToRating
        - [ ] (public fn) RatingToDbStr
        - [ ] (public fn) StrToRating
        - [ ] (public fn) RatingToStr
        - [ ] (public fn) DbStrToVuln
        - [ ] (public fn) VulnToDbStr
        - [ ] (public fn) StrToVuln
        - [ ] (public fn) VulnToStr
    - [ ] **DebuffList.cls**
        - [ ] (sub) Class_Initialize
        - [ ] (sub) Class_Terminate
        - [ ] num, capacity
        - [ ] debuffs()
        - [ ] spells()
        - [ ] actives()
        - [ ] numActive, numActiveLife
        - [ ] (public prop getter) Count
        - [ ] (public prop getter) Enchantment
        - [ ] (public prop getter) Spell
        - [ ] (public prop getter) ActiveCount
        - [ ] (public prop getter) ActiveLifeCount
        - [ ] (public prop getter) Active
        - [ ] (public prop getter) ActiveLife
        - [ ] (public fn) isLife
        - [ ] (public fn) add
        - [ ] (public fn) indexOf
        - [ ] (public fn) indexOfBinarySearch
        - [ ] (public fn) remove
        - [ ] (public fn) removeExpired
        - [ ] (public fn) removeIndex
        - [ ] (fn) updateActives
        - [ ] (fn) compareEnch
        - [ ] (fn) ensureCapacity
    - [ ] **httpHandler.cls**
        - [x] _probably does not need to be reimplemented_
    - [ ] **Hub.bas**
        - [ ] (public) LowBuffThresh
        - [ ] (GLOBAL const) scMonster = 0, scPlayer = 1, scBoth = 2
        - [ ] (public) bLoadingSettings
        - [ ] (public) lifeSpellLevel : DecalControls.Choice
        - [ ] (public) creatureSpellLevel : DecalControls.Choice
        - [ ] (public) txtVulnDbaseCount : DecalControls.StaticText
        - [ ] (public) VulnDbase : clsVulnDbase [new]
        - [x] ...
        - [ ] FSO
        - [ ] PluginSite
        - [ ] SpellF
        - [ ] CharStats
        - [ ] WorldFilter
        - [ ] DecorationType, eDecorationType
        - [ ] DecorationColor, eDecorationColor
        - [ ] POINTAPI
        - [ ] ErrorLog
        - [ ] (subs &amp; functions)
            - [ ] writeXML
            - [ ] SpellTime
            - [ ] max
            - [ ] min
            - [ ] makeRect
            - [ ] makeRect2
            - [ ] setRect
            - [ ] setRect2
            - [ ] moveRect
            - [ ] setRectPos
            - [ ] rectEqual
            - [ ] rectDebugString
            - [ ] makePoint
            - [ ] makeSize
            - [ ] sPluginNameVer
            - [ ] wtcwMessage
            - [ ] wtcwWarning
            - [ ] wtcwError
            - [ ] wtcwCmdInfo
            - [ ] handleErr
            - [ ] wtcwDebug
            - [ ] wtcw
            - [ ] stripColorTags
            - [ ] (pri) WriteToChatWindow
    - [ ] **HUD.cls**
        - [ ] _canvas related_
        - [ ] _icon related_
        - [ ] _DecalRender objects_
        - [ ] _point/rect types_
        - [ ] _fonts_
        - [ ] _settings and flags_
        - [ ] (subs &amp; functions)
            - [ ] Class_Initialize
            - [ ] Class_Terminate
            - [ ] BeginRender
            - [ ] BeginText
            - [ ] EndRender
            - [ ] SetAlignRight
            - [ ] LineHeight
            - [ ] FontSize (get and let)
            - [ ] Enabled (get)
            - [ ] Disable
            - [ ] Dispose
            - [ ] Render (large)
            - [ ] AddLine
            - [ ] DrawIcon
            - [ ] WriteText
            - [ ] Luminance
            - [ ] adjustBkgdRect
            - [ ] HitCheck
            - [ ] SetPos
            - [ ] Pos (get and let)
            - [ ] RightEdge (get and let)
            - [ ] BackgroundColor (get and let)
    - [ ] **monster.cls**
        - [ ] _public vars_
        - [ ] _private vars_
        - [ ] Class_Initialize, Class_Terminate, Init
        - [ ] _functions and subs_
    - [ ] **Target Info.cls**
        - [ ] (large) <mark>TODO</mark>
    - [ ] **Target Info.vbp**
        - [ ] (project file) <mark>TODO</mark>
    - [ ] **Target Info.vbw**
        - [ ] (window locs/dims, probably)
    - [ ] **TargetInfo.res**
        - [ ] (compiled resource file)
- [ ] remove unneeded (e.g. server-related)
- [ ] read latest .XML
- [ ] add debug statements, particularly for showing object props in chat window
- [ ] build
- [ ] publish


## References

<!-- - [URL](https://8.8.8.8/) -->

- <mark>TODO</mark>: Decal docs, Virindi View docs, .NET/C# info, old Target Info
- [https://mega.nz/folder/L1MniCKJ#1dQCCFPc2ddcFILa_JGeZw](Big-Ass AC Archive)


## Usage

<mark>TODO</mark>


## Build

Load this code into Visual Studio and click _Build_.

<details><summary>Output Log (contains warnings)</summary>
<pre>
>------ Build started: Project: NewtInfo, Configuration: Debug|AnyCPU ------
C:\Program Files x86\Visual Studio\201x\Edition\MSBuild\Current\Bin\Microsoft.Common.CurrentVersion.targets(2203,5): warning MSB3270: There was a mismatch between the processor architecture of the project being built "MSIL" and the processor architecture of the reference "Decal.Adapter", "x86". This mismatch may cause runtime failures. Please consider changing the targeted processor architecture of your project through the Configuration Manager so as to align the processor architectures between your project and references, or take a dependency on references with a processor architecture that matches the targeted processor architecture of your project.
C:\Program Files x86\Visual Studio\201x\Edition\MSBuild\Current\Bin\Microsoft.Common.CurrentVersion.targets(2203,5): warning MSB3270: There was a mismatch between the processor architecture of the project being built "MSIL" and the processor architecture of the reference "VirindiViewService", "x86". This mismatch may cause runtime failures. Please consider changing the targeted processor architecture of your project through the Configuration Manager so as to align the processor architectures between your project and references, or take a dependency on references with a processor architecture that matches the targeted processor architecture of your project.
C:\Users\PR\Source\NewtInfo\VirindiViews\Wrapper_Decal.cs(293,10): warning CS1030: #warning: 'DECAL_INTEROP not defined, MetaViewWrappers.DecalControls.Control.LayoutPosition will not be available.'
C:\Users\PR\Source\NewtInfo\VirindiViews\Wrapper_Decal.cs(705,24): warning CS0618: 'SliderWrapper.SliderPostition' is obsolete: 'Use Position'
C:\Users\PR\Source\NewtInfo\VirindiViews\Wrapper_Decal.cs(709,17): warning CS0618: 'SliderWrapper.SliderPostition' is obsolete: 'Use Position'
  NewtInfo -> C:\Users\PR\Source\NewtInfo\bin\Debug\NewtInfo.dll

Build succeeded.
</pre>
</details>
